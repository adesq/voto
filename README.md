# Sistema de voto seguro - ADESQ
 Sistema para votaciones abiertas con infraestructura en docker y en el cloud.

# MIEMBROS DEL EQUIPO
 - Eduard Avellanet Reyes
 - Josue David Zambrano Perozo 
 - Pau Enjuanes Sumalla
 - Martín Justo Fernández
 
# OBJETIVOS
 - Ofrecer la posibilidad a los usuarios de crear votaciones y votar de forma privada y segura.
 - Ofrecer al público general la documentación necesaria para la creación e implementación del servicio con el menor costo posible.
 - Adquirir nuevos conocimientos útiles para el desarrollo de otros proyectos de forma profesional.

# CONOCIMIENTOS A ADQUIRIR DURANTE EL PROYECTO
Para la realización de este proyecto necesitaremos adquirir conocimientos nuevos sobre diversos elementos con los que no hemos trabajado anteriormente o únicamente hemos trabajado de forma básica.
Algunos ejemplos pueden ser los servicios en la nube de Amazon, Docker y algunos de sus contenedores (Varnish, Traefik, Lazy Docker, Trivy, Sshuttle).
Además, esperamos que a través del uso de herramientas ya utilizadas previamente, tales como Apache, PHP, Python o MySQL podamos adquirir nuevos conocimientos derivados de su uso en nuevos entornos.

# REPOSITORIO
Para facilitar el desarrollo, además el acceso a toda la documentación, configuraciones y código de este proyecto se ha creado un repositorio en GitHub. 
Enlace: https://gitlab.com/adesq/voto

